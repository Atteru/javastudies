import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import java.awt.TextField;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class Form1 {

   protected Shell shell;
   private Text textField1;

   private String tekst;

   /**
    * Launch the application.
    * 
    * @param args
    */
   public static void main(String[] args) {
      try {
         Form1 window = new Form1();
         window.open();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    * Open the window.
    */
   public void open() {
      Display display = Display.getDefault();
      createContents();
      shell.open();
      shell.layout();
      while (!shell.isDisposed()) {
         if (!display.readAndDispatch()) {
            display.sleep();
         }
      }
   }

   /**
    * Create contents of the window.
    */
   protected void createContents() {
      shell = new Shell();
      shell.setSize(638, 409);
      shell.setText("SWT Application");

      textField1 = new Text(shell, SWT.BORDER);
      textField1.setBounds(10, 25, 341, 22);

      Button btnNewButton = new Button(shell, SWT.NONE);
      btnNewButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseUp(MouseEvent e) {
            tekst = textField1.getText();
            MessageDialog.openConfirm(shell, tekst, "Hello world");

         }
      });
      btnNewButton.setBounds(390, 172, 123, 59);
      btnNewButton.setText("New Button");

   }
}
