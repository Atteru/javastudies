package pl.lodz.uni.math.java4;

import pl.lodz.uni.math.java4.sourcesfactory.DataSourceFactory;
import pl.lodz.uni.math.java4.sourcesfactory.Source;

public class Main {

   public static void main(String[] args) {
      // TODO Auto-generated method stub

      DataSourceFactory factory = new DataSourceFactory();
      factory.setSource(Source.DB);
      factory.getSource().selectAllUsers();
      factory.setSource(Source.WEBSERVICES);
      factory.getSource().selectAllUsers();
      factory.getSource().selectUserById(1);
   }

}
