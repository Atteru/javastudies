package pl.lodz.uni.math.java4.domain;


public class User {
   public int userId;
   int age;
   String name;
   String lastname;

   public User(int personId, int age, String name, String lastname) {
      userId = personId;
      this.age = age;
      this.name = name;
      this.lastname = lastname;
   }
}
