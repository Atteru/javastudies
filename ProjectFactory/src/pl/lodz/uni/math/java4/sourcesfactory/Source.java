package pl.lodz.uni.math.java4.sourcesfactory;

public enum Source {
   DB, XML, WEBSERVICES;
}
