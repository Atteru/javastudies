package pl.lodz.uni.math.java4.sourcesfactory.sources;

import pl.lodz.uni.math.java4.domain.User;
import pl.lodz.uni.math.java4.sourcesfactory.DataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ewojzie on 06.06.2017.
 */
public class WSSource implements DataSource {

   private static WSSource instance = new WSSource();
   List<User> user = new ArrayList<>();

   public static WSSource getInstance() {
      return instance;
   }

   @Override
   public User selectUserById(int userId) {
      return user.get(userId);
   }

   @Override
   public List<User> selectAllUsers() {
      return user;
   }

}
