package pl.lodz.uni.math.java4.sourcesfactory;

import pl.lodz.uni.math.java4.sourcesfactory.sources.DBSource;
import pl.lodz.uni.math.java4.sourcesfactory.sources.WSSource;
import pl.lodz.uni.math.java4.sourcesfactory.sources.XMLSource;

import java.util.HashMap;
import java.util.Map;

public class DataSourceFactory {

   private static DataSource instance;

   private static Map<Source, DataSource> hm = new HashMap<Source, DataSource>() {
      {
         put(Source.DB, DBSource.getInstance());
         put(Source.XML, XMLSource.getInstance());
         put(Source.WEBSERVICES, WSSource.getInstance());
      }
   };

   public void setSource(Source name)
   {
      instance =  hm.get(name);
   }
   public DataSource getSource(){
      return instance;
   }
}
