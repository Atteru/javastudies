package pl.lodz.uni.math.java4.sourcesfactory.sources;

import pl.lodz.uni.math.java4.domain.User;
import pl.lodz.uni.math.java4.sourcesfactory.DataSource;

import java.util.ArrayList;
import java.util.List;

public class DBSource implements DataSource {

   List<User> user = new ArrayList<>();
   private static DBSource instance = new DBSource();

   public static DBSource getInstance() {
      return instance;
   }

   @Override
   public User selectUserById(int userId) {
      return user.get(userId);
   }

   @Override
   public List<User> selectAllUsers() {
      return user;
   }
}