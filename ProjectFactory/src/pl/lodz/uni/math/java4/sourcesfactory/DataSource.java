package pl.lodz.uni.math.java4.sourcesfactory;

import pl.lodz.uni.math.java4.domain.User;

import java.util.List;


public interface DataSource {
   public User selectUserById(int userId);

   public List<User> selectAllUsers();

}
