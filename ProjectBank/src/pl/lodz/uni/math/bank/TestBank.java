package pl.lodz.uni.math.bank;

public class TestBank {

   public static void main(String[] args) {
      // TODO Auto-generated method stub
      Bank bankPKO = Bank.getInstance();

      bankPKO.addBankAddress("Grudziadz", "Kujawsko-Pomorskie", "86-300", "Pilsudzkiego", 23);
      System.out.println(bankPKO);
      System.out.println("");
      System.out.println("");
      System.out.println("");

      bankPKO.addClient(new Client("Kumitsu", "Makosak", 123456));
      bankPKO.addClient(new Client("Dupaaasdas", "Makosak", 123456));

      System.out.println(bankPKO.getBankClient().get(1));
      System.out.println(bankPKO.getBankClient().get(0));
      bankPKO.getBankClient().get(0).addAddress("Grudziadz", "Kuj-Pom", "86-300", "Pomorska", 22);
      System.out.println(bankPKO.getBankClient().get(0).clientAddress);
      bankPKO.getBankClient().get(0).addAccount("Oszczednosciowe", "1234567891012");
      // System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0));
      bankPKO.getBankClient().get(0).addAccount("Konto dla Mlodych", "1234567891013");
      // System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(1));
      bankPKO.getBankClient().get(1).addAccount("Konto dla Mlodych", "1234567891012");

      bankPKO.getBankClient().get(0).clientAccount.get(0).makeDeposit(3211);

      System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0).getAccountBalance());
      bankPKO.getBankClient().get(0).clientAccount.get(0).makeDeposit(123);
      System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0).getAccountBalance());

      bankPKO.getBankClient().get(0).clientAccount.get(0).makeDeposit(321);
      bankPKO.getBankClient().get(0).clientAccount.get(0).makeCheck(321);
      bankPKO.getBankClient().get(0).clientAccount.get(0).makeDeposit(321);
      bankPKO.getBankClient().get(0).clientAccount.get(0).makeDeposit(321);

//      System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0).showDepositHistory());
//      System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0).showCheckHistory());
      System.out.println(bankPKO.getBankClient().get(0).clientAccount.get(0).showTransactionHistory());

   }

}
