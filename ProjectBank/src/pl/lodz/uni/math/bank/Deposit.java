package pl.lodz.uni.math.bank;

public class Deposit {
   private int id;
   Account accountTo;
   double amount;
   private double balanceAfterTransaction;

   public Deposit(double amount, Account accountTo) {
      this.amount = amount;
      this.accountTo = accountTo;
      this.accountTo.setTransactionId(this.accountTo.getTransactionId() + 1);
      this.id = this.accountTo.getTransactionId();
      this.accountTo.addTransactionToHistory(this);
      doDeposit();
   };

   public void doDeposit() {
      balanceAfterTransaction = this.accountTo.getAccountBalance() + this.amount;
      this.accountTo.setAccountBalance(balanceAfterTransaction);
   }

   public String toString() {
      return "Id tranzakcji: " + this.id + " Na konto: " + this.accountTo.getAccountNumber() + " "
            + this.accountTo.getAccountName() + " wplynelo: " + this.amount + "zl Saldo po transakcji: "
            + this.balanceAfterTransaction + "zl\n";
   }

}
