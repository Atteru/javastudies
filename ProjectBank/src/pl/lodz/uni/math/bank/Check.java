package pl.lodz.uni.math.bank;

public class Check {
   private int id;
   private Account accountFrom;
   private double amount;
   private double balanceAfterTransaction;

   public Check(double amount, Account accountFrom) {
      this.amount = amount;
      this.accountFrom = accountFrom;
      this.accountFrom.setTransactionId(this.accountFrom.getTransactionId() + 1);
      this.id = this.accountFrom.getTransactionId();
      this.accountFrom.addTransactionToHistory(this);
      doWithdraw();
   };

   public void doWithdraw() {
      if (this.amount < this.accountFrom.getAccountBalance()) {
         accountFrom.setAccountBalance(accountFrom.getAccountBalance() - amount);
         this.balanceAfterTransaction = this.accountFrom.getAccountBalance() - amount;
         accountFrom.getCheckHistory().add(this);
      } else {
         System.out.println("Insufficient account balance");
      }
   }

   public String toString() {
      return "Id tranzakcji: " + this.id + " Na konto: " + this.accountFrom.getAccountNumber() + " "
            + this.accountFrom.getAccountName() + " wplynelo: " + this.amount + "zl Saldo po transakcji: "
            + this.balanceAfterTransaction + "zl\n";
   }
}
