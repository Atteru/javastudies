package pl.lodz.uni.math.bank;

import java.util.ArrayList;
import java.util.List;

public class Client {
   String firstName;
   String lastName;
   int clientNumber;
   Address clientAddress;
   List<Account> clientAccount = new ArrayList<Account>();

   public Client(String firstName, String lastName, int clientNumber) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.clientNumber = clientNumber;
   }

   public void addAddress(String city, String state, String zip, String street, int number) {
      clientAddress = new Address(city, state, zip, street, number);
   }

   public void addAccount(String accountName, String accountNumber) {
      clientAccount.add(new Account(accountName, accountNumber));
   }

   public String toString() {
      return this.firstName + " " + this.lastName + " " + this.clientNumber;
   }

}
