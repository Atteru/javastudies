package pl.lodz.uni.math.bank;

import java.util.ArrayList;
import java.util.List;

public class Account {
   private int transactionId = 0;
   private String accountName;
   private String accountNumber;
   private double accountBalance;
   private List<Deposit> depositHistory = new ArrayList<Deposit>();
   private List<Check> checkHistory = new ArrayList<Check>();
   private List<Object> transactionsHistory = new ArrayList<Object>();

   public String getAccountName() {
      return accountName;
   }

   public void setAccountName(String accountName) {
      this.accountName = accountName;
   }

   public String getAccountNumber() {
      return accountNumber;
   }

   public void setAccountNumber(String accountNumber) {
      this.accountNumber = accountNumber;
   }

   public List<Deposit> getDepositHistory() {
      return depositHistory;
   }

   public void setDepositHistory(List<Deposit> depositHistory) {
      this.depositHistory = depositHistory;
   }

   public double getAccountBalance() {
      return accountBalance;
   }

   public void setAccountBalance(double accountBalance) {
      this.accountBalance = accountBalance;
   }

   public List<Check> getCheckHistory() {
      return checkHistory;
   }

   public void setCheckHistory(List<Check> checkHistory) {
      this.checkHistory = checkHistory;
   }

   public Account(String accountName, String accountNumber) {
      this.accountName = accountName;
      this.accountNumber = accountNumber;
   }

   public void makeDeposit(double amount) {
      depositHistory.add(new Deposit(amount, this));
   }

   public void makeCheck(double amount) {
      new Check(amount, this);

   }

   public String showCheckHistory() {
      String string = "";
      if (checkHistory.size() != 0) {

         for (Check i : checkHistory) {
            string += i.toString();
         }
      } else {
         string += "Your check history is empty";
      }
      return string;
   }

   public String showDepositHistory() {
      String string = "";
      if (depositHistory.size() != 0) {

         for (Deposit i : depositHistory) {
            string += i.toString();
         }
      } else {
         string += "Your Deposit history is empty";
      }
      return string;
   }

   public String toString() {
      return this.accountName + " " + this.accountNumber;

   }

   public int getTransactionId() {
      return transactionId;
   }

   public void setTransactionId(int transactionId) {
      this.transactionId = transactionId;
   }

   
   public void addTransactionToHistory(Object transaction) {
      transactionsHistory.add(transaction);
   }

   public String showTransactionHistory() {
      String string = "";
      if (transactionsHistory.size() != 0) {

         for (Object i : transactionsHistory) {
            string += i.toString();
         }
      } else {
         string += "Your transaction history is empty";
      }
      return string;
   }

}
