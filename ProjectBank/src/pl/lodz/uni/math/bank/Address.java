package pl.lodz.uni.math.bank;

public class Address {
   String city;
   String state;
   String zip;
   String street;
   int number;

   public Address(String city, String state, String zip, String street, int number) {
      this.city = city;
      this.state = state;
      this.zip = zip;
      this.street = street;
      this.number = number;
   }

   public String toString() {
      return this.state + " " + this.zip + " " + this.city + " " + this.street + " " + this.number;
   }
}
