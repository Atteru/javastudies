package pl.lodz.uni.math.bank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Bank {
   private static Bank bank = null;
   private String name;
   private Address bankAddress;
   private List<Client> bankClient = new ArrayList<Client>();

   public List<Client> getBankClient() {
      return bankClient;
   }

   private Bank() {
   }

   public static Bank getInstance() {
      if (bank == null) {
         bank = new Bank();
         bank.setName();
      }
      return bank;
   }

   public void addBankAddress(String city, String state, String zip, String street, int number) {
      bankAddress = new Address(city, state, zip, street, number);
   }

   public void addClient(Client client) {
      bankClient.add(client);
   }

   public String getName() {
      return name;
   }

   public void setName() {
      Scanner scan = new Scanner(System.in);
      name = "PKO BP";//scan.nextLine();
      scan.close();
   }

   public String toString() {
      return this.name + " " + this.bankAddress.city + " " + this.bankAddress.state + " " + this.bankAddress.street
            + " " + this.bankAddress.number;
   }

}