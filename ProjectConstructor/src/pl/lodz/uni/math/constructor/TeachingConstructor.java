package pl.lodz.uni.math.constructor;

public class TeachingConstructor extends BaseConstructor implements GlobalConstructor {
	public OtherConstructor other;

	public TeachingConstructor(int x) {
		System.out.println("Block A - konstruktor argumentowy klasy pochodnej");
	};

	public TeachingConstructor() {
		System.out.println("Block B - konstruktor bezargumentowy klasy pochodnej");
		other = new OtherConstructor();
	};

	{
		System.out.println("Block C - {} w klasie pochodnej");
	}
	static {
		System.out.println("Block D - static w klasie pochodnej");
	}

	public void CoolFunction() {
	};
}
