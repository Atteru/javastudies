package pl.lodz.uni.math.constructor;

public class BaseConstructor {
   OtherConstructor other;

   public BaseConstructor() {
      System.out.println("Blok B-1 - konstruktor klasy bazowej");
      other = new OtherConstructor();
   }

   {
      System.out.println("Blok B-2 - {} w klasie bazowej");
   }
   static {
      System.out.println("Blok B-3 - static w klasie bazowej");
   }

}
