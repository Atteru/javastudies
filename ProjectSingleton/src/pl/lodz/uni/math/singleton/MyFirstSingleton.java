package pl.lodz.uni.math.singleton;

public class MyFirstSingleton {
   private static MyFirstSingleton instance = new MyFirstSingleton(); // or null

   private MyFirstSingleton() {
      System.out.println("YES");
   }

   public static MyFirstSingleton getInstance() {
      System.out.println("Instance");

//      if (instance == null) {
//         instance = new MyFirstSingleton(); // lazy singleton 
//      }

      return instance;
   }

   private int x;

   public void setX(int x) {
      this.x = x;
   }

   public int getX() {
      return x;
   }

   static {
      System.out.println("YES!!!!!!");
   }
   {
      System.out.println("Init"); //Blok inicjujący wywołuje się zawsze przed konstruktorem!!!!!!!!!!!!
   }

}
