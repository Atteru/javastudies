package pl.lodz.uni.math.transactions;

import pl.lodz.uni.math.data.Account;
import pl.lodz.uni.math.exceptions.CheckException;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Check implements Transaction {
   private int id;
   Account accountFrom;
   double amount;
   private double balanceAfterTransaction;

   public Check(Account account, double amount) {
      this.id = account.transactionId.getAndIncrement();
      this.accountFrom = account;
      this.amount = amount;
   }

   @Override
   public void doTransaction() throws CheckException {
      if ( accountFrom.getAccountBalance() > amount) {
         accountFrom.setAccountBalance(accountFrom.getAccountBalance() - amount);
         balanceAfterTransaction = accountFrom.getAccountBalance();
      } else{
         throw new CheckException("Insufficent resources on account balance ");
      }
   }

   @Override
   public String toString() {
      return  "id=" + id +
              ", accountFrom=" + accountFrom +
              ", amount=" + amount +
              ", balanceAfterTransaction=" + balanceAfterTransaction;
   }


}
