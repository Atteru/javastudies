package pl.lodz.uni.math.transactions;

import pl.lodz.uni.math.data.Account;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Transfer implements  Transaction{
   private final Account accountFrom;
   private final Account accountTo;
   private final double amount;


   public Transfer(Account accountFrom, Account accountTo, double amount) {
      this.accountFrom = accountFrom;
      this.accountTo = accountTo;
      this.amount = amount;


   }
   @Override
   public void doTransaction() {

   }

}
