package pl.lodz.uni.math.transactions;

import pl.lodz.uni.math.data.Account;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Deposit implements Transaction {
   private int id;
   Account accountTo;
   double amount;
   private double balanceAfterTransaction;

   public Deposit(Account account, double amount) {
      this.id = account.transactionId.getAndIncrement();
      this.accountTo = account;
      this.amount = amount;
   }
   @Override
   public void doTransaction() {
      accountTo.setAccountBalance(accountTo.getAccountBalance()+amount);
      balanceAfterTransaction = accountTo.getAccountBalance();
      accountTo.addTransactionToAccountHistory(this);
   }

   @Override
   public String toString() {
      return  "Id transakcji: " + id +
              ", Kwota transakcji: " + amount +
              ", Stan po transakcji: " + balanceAfterTransaction;
   }


}
