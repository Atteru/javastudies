package pl.lodz.uni.math.transactions;

import pl.lodz.uni.math.exceptions.CheckException;

/**
 * Created by ewojzie on 08.06.2017.
 */
public interface Transaction {
   void doTransaction() throws CheckException;

}
