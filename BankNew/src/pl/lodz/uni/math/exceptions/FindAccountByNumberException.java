package pl.lodz.uni.math.exceptions;

/**
 * Created by atterek on 6/10/17.
 */
public class FindAccountByNumberException extends Exception {
    public FindAccountByNumberException(String s) {
        super(s);
    }
}
