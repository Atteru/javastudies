package pl.lodz.uni.math.exceptions;

/**
 * Created by atterek on 6/10/17.
 */
public class FindClientByNumberException extends Exception {

    public FindClientByNumberException(String s) {
        super(s);
    }
}
