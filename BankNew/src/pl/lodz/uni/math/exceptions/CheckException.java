package pl.lodz.uni.math.exceptions;

/**
 * Created by ewojzie on 09.06.2017.
 */
public class CheckException extends Exception{

   public CheckException(String message) {
      super(message);
   }

}
