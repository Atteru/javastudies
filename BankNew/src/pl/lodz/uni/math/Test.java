package pl.lodz.uni.math;

import com.sun.org.apache.xpath.internal.SourceTree;
import pl.lodz.uni.math.data.Bank;
import pl.lodz.uni.math.exceptions.CheckException;
import pl.lodz.uni.math.exceptions.FindAccountByNumberException;
import pl.lodz.uni.math.exceptions.FindClientByNumberException;
import pl.lodz.uni.math.helper.PrintData;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Test {
   public static void main(String[] args) {


      Bank bank = Bank.getBank();
      bank.setName("PKO");
      bank.addBankAddress("Lodz","Lodzkie","91-050","Poniatowskiego",37);
      bank.addBankClient("Ku", "Mitsu", 123456);
      String accountNumber = "14 1234 2345 3456 4567";
      try {
         bank.addBankAccount("Biznes", accountNumber,bank.findClientByNumber(123456));
      } catch (FindClientByNumberException e) {
         System.out.println(e.getMessage());
      }
      try {
         bank.findClientByNumber(123456).addClientAddress("Lodz","Lodzkie","91-050","Poniatowskiego", 123);
      } catch (FindClientByNumberException e) {
         System.out.println(e.getMessage());
      }

      try {
         bank.makeDeposit(accountNumber,100);
         bank.makeDeposit(accountNumber, 200);
         bank.makeDeposit(accountNumber, 200);
         bank.makeDeposit(accountNumber, 200);
      } catch (CheckException | FindAccountByNumberException e) {
         System.out.println(e.getMessage());
      }
      try {
         bank.makeCheck(accountNumber, 200);
      } catch (CheckException | FindAccountByNumberException e ) {
         System.out.println(e.getMessage());
      }

      System.out.println(bank);
      try {
         System.out.println(bank.findClientByNumber(123456));
      } catch (FindClientByNumberException e) {
         System.out.println(e.getMessage());
      }
      try {
         System.out.println(bank.findAccountByNumber(accountNumber));
      } catch (FindAccountByNumberException e) {
         System.out.println(e.getMessage());
      }
      PrintData printTransactionHistory = new PrintData();

      try {
         printTransactionHistory.showHistoryOfTransactions(bank.getBankAccount(accountNumber).getHistoryOfTransactions());
      } catch (FindAccountByNumberException e) {
         System.out.println(e.getMessage());
      }
   }
}
