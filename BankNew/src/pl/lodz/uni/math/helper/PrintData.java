package pl.lodz.uni.math.helper;

import pl.lodz.uni.math.transactions.Transaction;

import java.util.List;

/**
 * Created by atterek on 6/10/17.
 */
public class PrintData {

    public static void showHistoryOfTransactions(List<Transaction> historyOfTransactions) {
        System.out.println("Historia transakcji: ");
        for (Transaction historyOfTransaction : historyOfTransactions) {
            System.out.println(historyOfTransaction);
        }
    }
}
