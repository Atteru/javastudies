package pl.lodz.uni.math.data;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Client {
   String firstName;
   String lastName;
   int clientNumber;
   Address clientAddress;


   public Client(String firstName, String lastName, int clientNumber) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.clientNumber = clientNumber;
   }
   public void addClientAddress(String city, String state, String zip, String street, int number) {
      clientAddress = new Address(city, state, zip, street, number);
   }


   @Override
   public String toString() {
      return  "Klient: " + "\n" +
              "- Imie: " + firstName +
              ", Nazwisko: " + lastName +
              ", Numer klienta: " + clientNumber;
   }
}
