package pl.lodz.uni.math.data;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Address {
   String city;
   String state;
   String zip;
   String streetName;
   int number;

   public Address(String city, String state, String zip, String streetName, int number) {
      this.city = city;
      this.state = state;
      this.zip = zip;
      this.streetName = streetName;
      this.number = number;
   }

   @Override
   public String toString() {
      return  "Miasto: " + city +
              ", Wojewodztwo: " + state +
              ", Kod pocztowy: " + zip +
              ", Ulica: " + streetName +
              " " + number;
   }
}
