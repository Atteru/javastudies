package pl.lodz.uni.math.data;

import pl.lodz.uni.math.transactions.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Account {
   String accountName;
   String accountNumber;
   double accountBalance;
   Client client;
   public AtomicInteger transactionId = new AtomicInteger(1);
   private List<Transaction> historyOfTransactions = new ArrayList<>();


   public Account(String accountName, String accountNumber, Client client) {
      this.accountName = accountName;
      this.accountNumber = accountNumber;
      this.accountBalance = 0;
      this.client = client;
   }

   public void addTransactionToAccountHistory(Transaction transaction) {
      historyOfTransactions.add(transaction);
   }

   public List<Transaction> getHistoryOfTransactions() {
      return Collections.unmodifiableList(historyOfTransactions);
   }

   public double getAccountBalance() {
      return accountBalance;
   }

   public void setAccountBalance(double accountBalance) {
      this.accountBalance = accountBalance;
   }

   @Override
   public String toString() {
      return "Konto: " + "\n" +
              "- Imie: " + client.firstName+
              " Nazwisko: " + client.lastName+
              " Typ konta: " + accountName +
              " Numer konta: " + accountNumber +
              " Stan konta: " + accountBalance;
   }


}
