package pl.lodz.uni.math.data;

import pl.lodz.uni.math.exceptions.CheckException;
import pl.lodz.uni.math.exceptions.FindAccountByNumberException;
import pl.lodz.uni.math.exceptions.FindClientByNumberException;
import pl.lodz.uni.math.transactions.Check;
import pl.lodz.uni.math.transactions.Deposit;
import pl.lodz.uni.math.transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ewojzie on 08.06.2017.
 */
public class Bank {

   private static Bank bank = new Bank();
   private String name;
   private Address bankAddress;
   private List<Client> bankClient = new ArrayList<>();
   private List<Account> bankAccount = new ArrayList<>();

   private Bank() {
   }

   public static Bank getBank() {
      return bank;
   }

   public void addBankAddress(String city, String state, String zip, String street, int number) {
      bankAddress = new Address(city, state, zip, street, number);
   }

   public void addBankClient(String firstName, String lastNAme, int clientNumber) {
      bankClient.add(new Client(firstName, lastNAme, clientNumber));
   }

   public void addBankAccount(String accountName, String accountNumber, Client client) {
      bankAccount.add(new Account(accountName, accountNumber, client));
   }

   public void makeDeposit(String accountTo, double amount) throws CheckException, FindAccountByNumberException {
      Account account = findAccountByNumber(accountTo);
      if (account.equals(null)) {
          throw new FindAccountByNumberException("Nie ma takiego konta");
      }
      else{
          Transaction transaction = new Deposit(account, amount);
          transaction.doTransaction();
      }
   }

   public void makeCheck(String accountFrom, double amount) throws CheckException, FindAccountByNumberException {
      Account account = findAccountByNumber(accountFrom);
      if (account.equals(null)){
          throw new FindAccountByNumberException("Nie ma takiego konta");
      } else{
          Transaction transaction = new Check(account, amount);
          transaction.doTransaction();
      }
   }

    public Account getBankAccount(String number) throws FindAccountByNumberException {
      if (findAccountByNumber(number).equals(null)){
          throw new FindAccountByNumberException("Nie ma takiego konta");
      }else
          return findAccountByNumber(number);
   }

   public void setName(String name) {
      this.name = name;
   }


   @Override
   public String toString() {
      return  "Bank: " + "\n" +
              "- Nazwa: " + name +
              ", Address: " + bankAddress.toString();
   }

   public Client findClientByNumber(int number) throws FindClientByNumberException {
       for (Client client : bankClient) {
         if (client.clientNumber == number) {
            return client;
         }
      }
        throw new FindClientByNumberException("Nie ma takiego klienta");
   }

   public Account findAccountByNumber(String number) throws FindAccountByNumberException {
      for (Account account : bankAccount) {
         if (account.accountNumber.equals(number)) {
            return account;
         }
      }
       throw new FindAccountByNumberException("Nie ma takiego konta");
   }
}
